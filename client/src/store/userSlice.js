import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const fetchPostRegister = createAsyncThunk(
    'fields/fetchPostRegister',
    async (user) => {
        const response = await fetch(`/api/register`, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        })
        return response.json()
    },
)

const fetchPostAuthentication = createAsyncThunk(
    'fields/fetchPostAuthentication',
    async (user) => {
        const response = await fetch(`/api/login`, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        })
        return response.json()
    },
)

const fetchPostChangePassword = createAsyncThunk(
    'fields/fetchPostChangePassword',
    async (user) => {
        const response = await fetch(`/api/updatePassword`, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token
            },
            body: JSON.stringify(user)
        })
        return response.json()
    },
)

const userSlice = createSlice(
    {
        name: 'user',
        initialState: {
                userInfo: JSON.parse(localStorage.getItem('user')) || {},
                registration: false,
                authentication: false,
                changePassword: false,
        },
        reducers: {
            exit: (state) => {
                localStorage.clear()
                state.userInfo = {}
                state.registration = false
                state.authentication= false
                state.changePassword = false
            },
        },
            extraReducers: (builder) => {
                    builder.addCase(fetchPostRegister.fulfilled, (state, action) => {
                        if (action.payload.message === 'User already exists') {
                            alert('Пользователь с таким именем уже зарегистрирован')
                            state.registration = false
                        } else {
                            alert('Регистрация прошла успешно. Войдите в личный кабинет')
                            state.registration = true
                        }
                    })
                builder.addCase(fetchPostAuthentication.fulfilled, (state, action) => {
                    switch (action.payload.message) {
                        case 'User does not exist':
                            state.authentication = false
                            alert('Пользователь с таким именем не существует')
                            break
                        case 'Invalid password':
                            state.authentication = false
                            alert('Неверный пароль')
                            break
                        case 'Authenticated':
                            state.authentication = true
                            state.userInfo = {
                                id: action.payload.id,
                                username: action.payload.username,
                                token: action.payload.token
                            }
                            localStorage.setItem('user', JSON.stringify(action.payload))
                            break
                    }
                })
                builder.addCase(fetchPostChangePassword.fulfilled, (state, action) => {
                    if (action.payload.message === 'Invalid password') {
                        state.changePassword = false
                        alert('Неверный пароль')
                    } else {
                        state.changePassword = true
                        alert('Пароль успешно изменен')
                    }
                })
            },
    }
)

export const {
    exit,
} = userSlice.actions;

export {fetchPostRegister, fetchPostAuthentication, fetchPostChangePassword}
export default userSlice.reducer;
