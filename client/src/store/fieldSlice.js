import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const fetchGetFields = createAsyncThunk(
    'fields/fetchGetFields',
    async (token) => {
        const response = await fetch(`/api/fields`, {
            method: "get",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }})
        return response.json()
        }
)

const fetchGetUserFields = createAsyncThunk(
    'fields/fetchGetUserFields',
    async (data) => {
        const response = await fetch(`/api/fields/${data.id}`, {
            method: "get",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + data.token
            }})
        return response.json()
    },
)
const fetchPostAddField = createAsyncThunk(
    'fields/fetchPostAddField',
    async (data) => {
        const response = await fetch(`/api/addField`, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + data.token
            },
            body: JSON.stringify(data)
        })
        return response.json()
    },
)

const fetchPostUpdateField = createAsyncThunk(
    'fields/fetchPostUpdateField',
    async (data) => {
        const response = await fetch(`/api/updateField`, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + data.token
            },
            body: JSON.stringify(data)
        })
        return response.json()
    },
)

const fetchDeleteField = createAsyncThunk(
    'fields/fetchDeleteField',
    async (data) => {
        const response = await fetch(`/api/deleteField/${data.id}`, {
            method: "delete",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + data.token
            },
        })
        return response.json()
    },
)

const fetchPostAddDeposit = createAsyncThunk(
    'fields/fetchPostAddDeposit',
    async (data) => {
        const response = await fetch(`/api/addDeposit`, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + data.token
            },
            body: JSON.stringify(data)
        })
        return response.json()
    },
)

const fieldSlice = createSlice(
    {
        name: 'field',
        initialState: {
            fields: JSON.parse(localStorage.getItem('fields')) || [],
            fluidTypes: [
                {id: 1, name: 'Нефтяное'},
                {id: 2, name: 'Газонефтяное'},
                {id: 3, name: 'Нефтегазоконденсатное'},
                {id: 4, name: 'Нефтегазовое'},
            ],
            collectorTypes: [
                {id: 1, name: 'терригенный'},
                {id: 2, name: 'карбонатный'}
            ],
            userFields: [],
            sorting: {
                ascendingFields: false,
                descendingFields: false,
                sortFluidType: '',
            },
            activeFieldMap: '',
            activeFluidTypeMap: '',
            activeDepositMap: '',
            activeCollectorTypeMap: ''
        },
        reducers: {
            sortAscendingFields: (state) => {
                state.fields.sort((a, b) => a.name.localeCompare(b.name))
                state.userFields.sort((a, b) => a.name.localeCompare(b.name))
                state.sorting.ascendingFields = true
                state.sorting.descendingFields = false
            },
            sortDescendingFields: (state) => {
                state.fields.sort((a, b) => b.name.localeCompare(a.name))
                state.userFields.sort((a, b) => b.name.localeCompare(a.name))
                state.sorting.descendingFields = true
                state.sorting.ascendingFields = false
            },
            resetSorting: (state) => {
                state.fields.sort((a, b) => a.name.localeCompare(b.name))
                state.sorting.descendingFields = false
                state.sorting.ascendingFields = false
                state.sorting.sortFluidType = ''
            },
            sortFluidType: (state, action) => {
                state.sorting.sortFluidType = action.payload.type
            },
            setActiveFieldMap: (state, action) => {
                state.activeFieldMap = action.payload;
                state.activeFluidTypeMap = {id: state.activeFieldMap.fluidTypeId, name: state.activeFieldMap.fluidType}
                if (state.activeFieldMap.deposits.length) {
                    state.activeDepositMap = state.activeFieldMap.deposits[0];
                    state.activeCollectorTypeMap = {id: state.activeFieldMap.deposits[0].collectorTypeId, name: state.activeFieldMap.deposits[0].collectorType}
                } else {
                    state.activeDepositMap = '';
                    state.activeCollectorTypeMap = ''
                }
            },
            setActiveFluidTypeMap: (state, action) => {
                state.activeFluidTypeMap = action.payload;
            },
            setActiveDepositMap: (state, action) => {
                state.activeDepositMap = action.payload;
            },
            setActiveCollectorTypeMap: (state, action) => {
                state.activeCollectorTypeMap = action.payload;
            },
            resetActiveField: (state) => {
                state.activeFieldMap = ''
                state.activeDepositMap = ''
                state.activeFluidTypeMap = ''
                state.activeCollectorTypeMap = ''
            },
        },
        extraReducers: (builder) => {
            builder.addCase(fetchGetFields.fulfilled, (state, action) => {
                localStorage.setItem('fields', JSON.stringify(action.payload))
                state.fields = action.payload
            })
            builder.addCase(fetchGetUserFields.fulfilled, (state, action) => {
                state.userFields = action.payload
            })
        },
    }
)

export const {
    sortAscendingFields,
    sortDescendingFields,
    resetSorting,
    sortFluidType,
    setActiveFieldMap,
    setActiveFluidTypeMap,
    setActiveDepositMap,
    setActiveCollectorTypeMap,
    resetActiveField
} = fieldSlice.actions;

export {fetchGetFields, fetchGetUserFields, fetchPostAddField, fetchDeleteField, fetchPostUpdateField, fetchPostAddDeposit}
export default fieldSlice.reducer;
