import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const fetchGetSubsoilObjectsBySubject = createAsyncThunk(
    'fields/fetchGetSubsoilObjectsBySubject',
    async (data) => {
            const response = await fetch(`/api/subsoilsBySubject/${data.id}`, {
                method: "get",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + data.token
                }})
            return response.json()
    },
)

const fetchPostSearchAnalogues = createAsyncThunk(
    'fields/fetchPostSearchAnalogues',
    async (data) => {
        const response = await fetch(`/api/search`, {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + data.token
            },
            body: JSON.stringify(data)
        })
        return response.json()
    },
)

const fetchGetHistorySearch = createAsyncThunk(
    'fields/fetchGetHistorySearch',
    async (data) => {
        const response = await fetch(`/api/history/${data.id}`, {
            method: "get",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + data.token
            }})
        return response.json()
    },
)

const searchSlice = createSlice(
    {
        name: 'search',
        initialState: {
                searchParameters: [
                        {id: 1, name: 'Поиск по всем объектам'},
                        {id: 2, name: 'Поиск в пределах субъекта'},
                ],
                activeSearchParameter: {id: 1, name: 'Поиск по всем объектам'},
                subsoilObjectsIdsBySubject: [],
                analogues: [],
                historySearch: []
        },
        reducers: {
                setSearchParameter: (state, action) => {
                        state.activeSearchParameter = action.payload;
                        state.subsoilObjectsIdsBySubject = []
                        state.analogues = []
                },
                resetSearchParameter: (state) => {
                        state.activeSearchParameter = {id: 1, name: 'Поиск по всем объектам'}
                        state.subsoilObjectsIdsBySubject = []
                        state.analogues = []
                },
        },
        extraReducers: (builder) => {
                builder.addCase(fetchGetSubsoilObjectsBySubject.fulfilled, (state, action) => {
                        state.subsoilObjectsIdsBySubject = action.payload;
                })
                builder.addCase(fetchPostSearchAnalogues.fulfilled, (state, action) => {
                        state.analogues = action.payload
                        if (!state.analogues.length) {
                            alert('Аналогов не найдено')
                        } else {
                            state.subsoilObjectsIdsBySubject = []
                        }

                })
                builder.addCase(fetchGetHistorySearch.fulfilled, (state, action) => {
                        state.historySearch = action.payload
                })
        },
    }
)

export const {
        setSearchParameter,
        resetSearchParameter,
} = searchSlice.actions;

export {fetchGetSubsoilObjectsBySubject, fetchPostSearchAnalogues, fetchGetHistorySearch}
export default searchSlice.reducer;
