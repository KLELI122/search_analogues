import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const fetchGetSubsoilObjects = createAsyncThunk(
    'subsoilObjects/fetchGetSubsoilObjects',
    async (token) => {
        const response = await fetch('/api/subsoils', {
            method: "get",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }})
        return response.json()
    },
)

const mapSlice = createSlice(
    {
        name: 'map',
        initialState: {
            isLoading: false,
            subsoilObjects: [],
            activeSubsoilObject: '',
        },
        reducers: {
            setActiveObject: (state, action) => {
                state.activeSubsoilObject = action.payload
            },
            resetActiveObject: (state) => {
                state.activeSubsoilObject = ''
            },
        },
        extraReducers: (builder) => {
            builder.addCase(fetchGetSubsoilObjects.pending, (state) => {
                state.isLoading = true
            })
            builder.addCase(fetchGetSubsoilObjects.fulfilled, (state, action) => {
                state.subsoilObjects = action.payload
                state.isLoading = false
            })
        },
    }
)

export const {
    setActiveObject,
    resetActiveObject
} = mapSlice.actions;

export {fetchGetSubsoilObjects}
export default mapSlice.reducer;
