import { configureStore } from '@reduxjs/toolkit';
import userSlice from "./userSlice";
import mapSlice from "./mapSlice";
import fieldSlice from "./fieldSlice";
import searchSlice from "./searchSlice";

const store = configureStore({
    reducer: {
        user: userSlice,
        map: mapSlice,
        field: fieldSlice,
        search: searchSlice,
    }
});
export default store;