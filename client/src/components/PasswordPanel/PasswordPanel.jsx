import scss from './PasswordPanel.module.scss'
import Input from "../Input/Input";
import Button from "../Button/Button";
import Close from "../Icons/Close";
import {useEffect, useState} from "react";
import {fetchPostChangePassword} from "../../store/userSlice";
import {useDispatch, useSelector} from "react-redux";

const PasswordPanel = ({setIsOpen}) => {

    const [oldPassword, setOldPassword] = useState('')
    const [isErrorOldPassword, setIsErrorOldPassword] = useState('')

    const [newPassword, setNewPassword] = useState('')
    const [isErrorNewPassword, setIsErrorNewPassword] = useState('')

    const changeValuePassword = (e) => {
        switch (e.target.name) {
            case 'oldPassword':
                e.target.value === '' ? setIsErrorOldPassword('Заполните поле') : setIsErrorOldPassword('')
                setOldPassword(e.target.value)
                break;
            case 'newPassword':
                e.target.value.length < 8 ? setIsErrorNewPassword('Пароль должен содержать не менее 8 символов') : setIsErrorNewPassword('')
                setNewPassword(e.target.value)
                break;
            default:
                break;
        }
    }

    const [isDisabledBtn, setIsDisabledBtn] = useState(true)

    useEffect(() => {
        oldPassword && newPassword && setIsDisabledBtn(isErrorOldPassword !== '' || isErrorNewPassword !== '')
    }, [oldPassword, newPassword, isErrorOldPassword, isErrorNewPassword]);

    const {userInfo} = useSelector(state => state.user);
    const dispatch = useDispatch();

    const sendPassword = (e) => {
        e.preventDefault()
        !isDisabledBtn && dispatch(fetchPostChangePassword({
            id: userInfo.id,
            token: userInfo.token,
            oldPassword: oldPassword,
            newPassword: newPassword,
        }))
    }

    const {changePassword} = useSelector(state => state.user)

    useEffect(() => {
        changePassword && setIsOpen(false)
    }, [changePassword]);

    return <div className={scss.passwordPanel}>
        <Input
            labelContent='Старый пароль:'
            inputName='oldPassword'
            inputType='password'
            changeValueState={changeValuePassword}
            isError={isErrorOldPassword}
            infoError={isErrorOldPassword}/>
        <Input
            labelContent='Новый пароль:'
            inputName='newPassword'
            inputType='password'
            changeValueState={changeValuePassword}
            isError={isErrorNewPassword}
            infoError={isErrorOldPassword}/>
        <Button
            content='Сменить пароль'
            onClick={sendPassword}
            isDisabled={isDisabledBtn}
            theme='dark'/>
        <div
            className={scss.closeBtn}
            onClick={() => {setIsOpen(false)}}>
            <Close/>
        </div>
    </div>
}

export default PasswordPanel;