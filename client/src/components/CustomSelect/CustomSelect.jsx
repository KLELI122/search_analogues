import Select from 'react-select'
import {useEffect, useState} from "react";

const CustomSelect = ({title, defOptions, currentValue, onChange, placeholder}) => {

    const [selectedOption, setSelectedOption] = useState(null)
    const [options, setOptions] = useState([])

    useEffect(() => {
        currentValue ? setSelectedOption({...currentValue, value: currentValue.name, label: currentValue.name}) : setSelectedOption(null)
    }, [currentValue]);

    useEffect(() => {
        const options = []
        defOptions.map(item => {
            options.push({...item, value: item.name, label: item.name})
        })
        setOptions(options)
    }, [defOptions]);

    const onChangeSelect = (event) => {
        onChange(event)
        setSelectedOption(event)
    }

    const customTheme = (theme) => ({
        ...theme,
        colors: {
            ...theme.colors,
            primary25: '#dcdcdc',
            primary: '#d8cbf5 ',
        },
    });

    const styles= {
        control: (baseStyles) => ({
            ...baseStyles,
            cursor: 'pointer',
        }),
    }

    return <div>
        <h4>{title}</h4>
        <Select
            options={options}
            onChange={(event) => onChangeSelect(event)}
            placeholder={placeholder ? placeholder : "Выберите..."}
            theme={customTheme}
            styles={styles}
            value={selectedOption && selectedOption}/>
    </div>
}
export default CustomSelect;
