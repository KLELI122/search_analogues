import scss from '../LoginPage/LoginPage.module.scss'
import {useState, useEffect} from "react";
import Input from "../Input/Input";
import Button from "../Button/Button";
import {useDispatch} from "react-redux";
import {fetchPostAuthentication} from "../../store/userSlice";

const SignInPanel = () => {

    const [login, setLogin] = useState('')
    const [isErrorSignInLogin, setIsErrorSignInLogin] = useState('')

    const [password, setPassword] = useState('')
    const [isErrorSignInPassword, setIsErrorSignInPassword] = useState('')

    const changeValueSignIn = (e) => {
        const inputValue = e.target.value
        switch (e.target.name) {
            case 'signInLogin':
                inputValue === '' ? setIsErrorSignInLogin('Заполните поле') : setIsErrorSignInLogin('')
                setLogin(inputValue)
                break;
            case 'signInPassword':
                inputValue === '' ? setIsErrorSignInPassword('Заполните поле') : setIsErrorSignInPassword('')
                setPassword(e.target.value)
                break;
        }
    }

    const [isDisabledBtn, setIsDisabledBtn] = useState(true)

    useEffect(() => {
        login && password && setIsDisabledBtn(isErrorSignInLogin !== '' || isErrorSignInPassword !== '')
    }, [login, password, isErrorSignInLogin, isErrorSignInPassword]);

    const dispatch = useDispatch()

    const sendSignIn = (e) => {
        e.preventDefault()
        !isDisabledBtn && dispatch(fetchPostAuthentication({username: login, password: password}))
    }

    return <form onSubmit={(e) => {sendSignIn(e)}}>
        <div className={scss.signInPanel}>
            <Input
                labelContent='Логин:'
                inputName='signInLogin'
                inputType='text'
                changeValueState={changeValueSignIn}
                isError={isErrorSignInLogin}/>
            <Input
                labelContent='Пароль:'
                inputName='signInPassword'
                inputType='password'
                changeValueState={changeValueSignIn}
                isError={isErrorSignInPassword}/>
        </div>
        <Button
            content='Войти'
            isDisabled={isDisabledBtn}
            theme='dark'/>
    </form>
}

export default SignInPanel;