import scss from'./FieldsPage.module.scss'
import {useDispatch, useSelector} from "react-redux";
import GroupField from "../GroupField/GroupField";
import {useState} from "react";
import Button from "../Button/Button";
import ControlPanel from "../ControlPanel/ControlPanel";
import Tab from "../Tab/Tab";
import UserFieldsPanel from "../UserFieldsPanel/UserFieldsPanel";
import {resetSorting} from "../../store/fieldSlice";

const FieldsPage = () => {

    const dispatch = useDispatch()
    const {fields, sorting} = useSelector((state) => state.field);
    const [numberVisible, setNumberVisible] = useState(20);
    const [isOpenFields, setIsOpenFields] = useState(true)
    const [isOpenUserFields, setIsOpenUserFields] = useState(false)

    const changeOpenTab = (setIsOpen) => {
        setIsOpenFields(false)
        setIsOpenUserFields(false)
        setIsOpen(true)
        dispatch(resetSorting())
    }

    return <main className={scss.fieldsPage}>
        <div className={scss.tabPanel}>
            <Tab
                label='База месторождений'
                isOpen={isOpenFields}
                onClick={() => changeOpenTab(setIsOpenFields)}/>
            <Tab
                label='Пользовательские месторождения'
                isOpen={isOpenUserFields}
                onClick={() => changeOpenTab(setIsOpenUserFields)}/>
        </div>
        {isOpenFields && <div className={scss.fieldsPanel}>
            {fields.slice(0, sorting.sortFluidType ? fields.length : numberVisible).map((field) => {
                if (!sorting.sortFluidType || field.fluidType === sorting.sortFluidType) {
                    return <div
                        key={field.id}
                        className={scss.group}>
                        <GroupField content={field}/>
                    </div>
                }
            })}
            {!sorting.sortFluidType && <div className={scss.btnPanel}>
                <Button
                    content='Показать еще'
                    onClick={() => {
                        setNumberVisible(numberVisible + 20)
                    }}
                    theme='light'/>
            </div>}
        </div>}
        {isOpenUserFields && <UserFieldsPanel />}
        <ControlPanel/>
    </main>
}

export default FieldsPage;