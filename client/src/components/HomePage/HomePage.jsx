import scss from './HomePage.module.scss'
import Button from "../Button/Button";
import {slider} from "../../utils/objectProperties.jsx";
import {CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import ArrowLeft from "../Icons/ArrowLeft";
import ArrowRight from "../Icons/ArrowRight";
import logo from '../../assets/logo.svg'
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";

const HomePage = () => {
    const {userInfo} = useSelector(state => state.user);

    return <main className={scss.homePage}>
        <div className={scss.homePagePanel}>
            <div className={scss.homePageInfo}>
                <div className={scss.title}>Сервис поиска аналогов залежей нефти</div>
                <div>Подбор аналогов для целевого объекта, выполняемый при обосновании запасов залежей нефти в задачах геологического и гидродинамического моделирования и проектирования разработки месторождений</div>
                <Link to={`${userInfo.token ? '/search' : '/login'}`}>
                    <Button
                        content='Начать поиск'
                        theme='dark'/>
                </Link>
            </div>
            <img src={logo} alt='home'/>
        </div>
        <div className={scss.slider}>
            <CarouselProvider
                naturalSlideWidth={100}
                naturalSlideHeight={125}
                totalSlides={3}>
                <ButtonBack className={scss.buttonLeft}><ArrowLeft /></ButtonBack>
                <ButtonNext className={scss.buttonRight}><ArrowRight /></ButtonNext>
                <Slider>
                    {slider.map(item => {
                        return <Slide
                            index={item.index}
                            key={item.title}>
                            <div className={scss.slide}>
                                <img src={item.img} alt='homeImg' />
                                <div className={scss.title}>{item.title}</div>
                            </div>
                        </Slide>
                    })}
                </Slider>
            </CarouselProvider>
        </div>
    </main>
}
export default HomePage;