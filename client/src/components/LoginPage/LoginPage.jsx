import scss from './LoginPage.module.scss'
import SignInPanel from "../SignInPanel/SignInPanel";
import SignUpPanel from "../SignUpPanel/SignUpPanel";
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {useNavigate} from "react-router";

const LoginPage = () => {

    const [isSignState, setSignState] = useState('signIn');
    const {registration, userInfo} = useSelector(state => state.user);

    useEffect(() => {
        registration && setSignState('signIn')
    }, [registration]);

    const navigate = useNavigate();

    useEffect(() => {
        userInfo.token && navigate(`/login/${userInfo.id}`);
    }, [userInfo]);

    return <main>
        <div className={scss.loginForm}>
            <div className={scss.loginMenu}>
                <div className={`${scss.signIn} ${isSignState === 'signIn' && scss.active}`}
                     onClick={() => { setSignState('signIn') }}>
                     Войти
                </div>
                <div className={`${scss.signUp} ${isSignState === 'signUp' && scss.active}`}
                     onClick={() => { setSignState('signUp')}}>
                     Зарегистрироваться
                </div>
            </div>
            {isSignState === 'signIn' && <SignInPanel />}
            {isSignState === 'signUp' && <SignUpPanel />}
        </div>
    </main>
}

export default LoginPage;