import scss from './UserFieldsPanel.module.scss'
import Button from '../Button/Button'
import {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import GroupField from "../GroupField/GroupField";
import UpdateFieldPanel from "../UpdateFieldPanel/UpdateFieldPanel";
import {fetchDeleteField, fetchGetUserFields} from "../../store/fieldSlice";

const UserFieldsPanel = () => {
    const dispatch = useDispatch();
    const {userInfo} = useSelector(state => state.user)
    const {userFields, sorting} = useSelector(state => state.field)
    const [isOpenAddPanel, setIsOpenAddPanel] = useState(false);
    const [currentField, setCurrentField] = useState('')

    const updateField = (filed) => {
        setIsOpenAddPanel(true)
        setCurrentField(filed)
    }

    const deleteField = async (id) => {
        await dispatch(fetchDeleteField({
            id: id,
            token: userInfo.token
        }))
        dispatch(fetchGetUserFields({id: userInfo.id, token: userInfo.token}))
    }

    return <div className={scss.userFieldsPanel}>
        {userFields && userFields.map(field => {
            if (!sorting.sortFluidType || field.fluidType === sorting.sortFluidType) {
                return <div className={scss.userField} key={field.id}>
                    <GroupField content={field}/>
                    <div className={scss.btnPanel}>
                        <Button content='Изменить' onClick={() => {updateField(field)}} theme='light'/>
                        <Button content='Удалить' onClick={() => {deleteField(field.id)}} theme='light'/>
                    </div>
                </div>
            }
        })}
        <div className={scss.addBtn}>
            <Button
                content='+ Добавить месторождение'
                onClick={() => {updateField('')}}
                theme='light'/>
        </div>
        {isOpenAddPanel && <UpdateFieldPanel
            setIsOpenPanel={setIsOpenAddPanel}
            currentField={currentField}/>}
    </div>
}

export default UserFieldsPanel;