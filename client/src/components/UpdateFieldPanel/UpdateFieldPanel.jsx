import scss from './UpdateFieldPanel.module.scss'
import Input from "../Input/Input";
import CustomSelect from "../CustomSelect/CustomSelect";
import Button from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {fetchGetUserFields, fetchPostAddField, fetchPostUpdateField} from "../../store/fieldSlice";
import {fetchGetSubsoilObjects} from "../../store/mapSlice";

const UpdateFieldPanel = ({setIsOpenPanel, currentField}) => {
    const dispatch = useDispatch();

    const {userInfo} = useSelector(state => state.user)

    const {subsoilObjects} = useSelector(state => state.map)
    const {fluidTypes} = useSelector(state => state.field)

    const [subsoilOptions, setSubsoilOptions] = useState([])

    useEffect(() => {
        setSubsoilOptions(subsoilObjects.filter(object => !object.fieldId))
    }, []);

    const [isDisabledBtn, setIsDisabledBtn] = useState(true)
    const [field, setField] = useState(currentField && {id: currentField.id, name: currentField.name} || '')
    const [fluid, setFluid] = useState(currentField && {id: currentField.fluidTypeId, name: currentField.fluidType} || '')
    const [subsoil, setSubsoil] = useState(currentField && {id: currentField.subsoilObjectId, name: currentField.subsoilObject} || '')


    const sendNewField = async () => {
       if (!isDisabledBtn) {
           await dispatch(fetchPostAddField({
               userId: userInfo.id,
               token: userInfo.token,
               nameField: field,
               fluidId: fluid.id,
               subsoilId: subsoil.id
           }))
           setIsOpenPanel(false)
           dispatch(fetchGetUserFields({id: userInfo.id, token: userInfo.token}))
           dispatch(fetchGetSubsoilObjects(userInfo.token))
       }
    }

    const sendUpdateField = async () => {
        if (!isDisabledBtn) {
            await dispatch(fetchPostUpdateField({
                userId: userInfo.id,
                token: userInfo.token,
                fieldId: currentField.id,
                nameField: field.name || field,
                fluidId: fluid.id,
                subsoilId: subsoil.id
            }))
            setIsOpenPanel(false)
            dispatch(fetchGetUserFields({id: userInfo.id, token: userInfo.token}))
            dispatch(fetchGetSubsoilObjects(userInfo.token))
        }
    }

    useEffect(() => {
        field && fluid && subsoil ? setIsDisabledBtn(false) :  setIsDisabledBtn(true)
    }, [field, fluid, subsoil]);


    return <div className={scss.addPanel}>
        <Input
            inputName='nameField'
            inputType='text'
            placeholder='Название месторождения'
            changeValueState={(e) => {setField(e.target.value)}}
            currentValue={{id: currentField.id, name: currentField.name}}
            hideError={true}/>
        <CustomSelect
            placeholder={'Объект недропользования'}
            defOptions={subsoilOptions}
            currentValue={currentField && {id: currentField.subsoilObjectId, name: currentField.subsoilObject}}
            onChange={(subsoil) => {setSubsoil(subsoil)}}/>
        <CustomSelect
            placeholder={'Тип флюида'}
            defOptions={fluidTypes}
            currentValue={currentField && {id: currentField.fluidTypeId, name: currentField.fluidType}}
            onChange={(fluid) => {setFluid(fluid)}}/>
        <div className={scss.btnPanel}>
            <Button
                content='Сохранить'
                theme={'light'}
                onClick={currentField ? sendUpdateField : sendNewField}
                isDisabled={isDisabledBtn}/>
            <Button
                content='Отменить'
                theme={'light'}
                onClick={() => {
                    setIsOpenPanel(false)
                }}/>
        </div>
    </div>
}

export default UpdateFieldPanel;