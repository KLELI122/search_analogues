import { MapContainer, TileLayer} from 'react-leaflet'
import {useSelector} from "react-redux";
import 'leaflet/dist/leaflet.css'
import './SubsoilMap.scss'
import MultiPolygon from '../MultiPolygon/MultiPolygon'

const SubsoilMap = () => {
    const {subsoilObjects} = useSelector(state => state.map);

    return <div className='mapContainer'>
        <MapContainer
            center={[65, 74]}
            zoom={3.5}
            scrollWheelZoom={true}
            zoomControl={false}>
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
            {subsoilObjects && subsoilObjects.map(item => {
                if (item.fieldId) {
                    return <MultiPolygon
                        key={item.id}
                        item={item}/>
                }
            })}
        </MapContainer>
    </div>
}

export default SubsoilMap;