import {Polygon, Tooltip} from "react-leaflet";
import {setActiveObject} from "../../store/mapSlice";
import {setActiveFieldMap} from "../../store/fieldSlice";
import {resetSearchParameter} from "../../store/searchSlice";
import {useDispatch, useSelector} from "react-redux";
import PopupPolygon from "../PopupPolygon/PopupPolygon";
import {useEffect, useRef} from "react";

const MultiPolygon = ({item}) => {

    const {activeSubsoilObject} = useSelector(state => state.map);
    const {subsoilObjectsIdsBySubject, analogues, activeSearchParameter} = useSelector(state => state.search);

    const checkObjectState = (defState, activeState, searchState, analogState) => {
        if (analogues.find(object => object.subsoilObjectId === item.id)) {
            return analogState
        } else if (subsoilObjectsIdsBySubject.find(object => object.id === item.id && item.id !== activeSubsoilObject.id)) {
            return searchState
        } else if (activeSubsoilObject.id === item.id) {
            return activeState
        } else {
            return defState
        }
    }

    const pathOptions = {
        color: checkObjectState('#b4a2d9', '#8a7b8a', '#8d9db6', '#667292'),
        fillOpacity: checkObjectState(0.3, 0.8, 0.8, 0.8),
        weight: 0.7,
    }

    const dispatch = useDispatch();

    const eventHandlers = {
        mouseover: function(){
            this.setStyle({
                fillOpacity: 0.8,
            })
        },
        mouseout: function(){
            this.setStyle({
                fillOpacity: checkObjectState(0.3, 0.8, 0.8, 0.8),
            })
        },
        click: function(){
            const field = fields.find(object => object.subsoilObjectId === item.id)
            dispatch(setActiveObject(item))
            dispatch(setActiveFieldMap(field))
            dispatch(resetSearchParameter())
        },
    }

    const {fields, activeFieldMap} = useSelector(state => state.field);
    const polygonRef = useRef(null)

    useEffect(() => {
        !activeFieldMap.id && polygonRef.current.closePopup()
        item.id === activeSubsoilObject.id && polygonRef.current.openPopup()
    }, [activeFieldMap]);

    useEffect(() => {
        analogues.length && polygonRef.current.closePopup()
    }, [analogues]);

    return <Polygon
            pathOptions={pathOptions}
            positions={item.geometry}
            eventHandlers={eventHandlers}
            ref={polygonRef}>
        <Tooltip sticky>{item.name}</Tooltip>
        <PopupPolygon object={item}/>
    </Polygon>
}

export default MultiPolygon;