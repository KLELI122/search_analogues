import scss from'./ResultPanel.module.scss'
import {useSelector} from "react-redux";
import {analogueItems, table} from "../../utils/objectProperties.jsx";

const ResulPanel = () => {
    const {analogues} = useSelector(state => state.search);

    return <div className={scss.resultPanel}>
        <div className={scss.resultObjects}>
            <table >
                <caption>Результаты поиска</caption>
                <thead>
                <tr>
                    {table.map(item => {
                        return <th scope="col" key={item}>{item}</th>
                    })}
                </tr>
                </thead>
                <tbody>
                {analogues.map((analogue) => {
                    return <tr key={analogue.id}>
                        {analogueItems(analogue).map(item => {
                            return <th scope="row" key={item.title}>{item.content}</th>
                        })}
                    </tr>
                })}
                </tbody>
            </table>
        </div>
    </div>
}

export default ResulPanel;