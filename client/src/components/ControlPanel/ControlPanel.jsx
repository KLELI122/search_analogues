import scss from './ControlPanel.module.scss'
import Button from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {resetSorting, sortAscendingFields, sortDescendingFields, sortFluidType} from "../../store/fieldSlice";

const ControlPanel = () => {
    const {fluidTypes, sorting} = useSelector(state => state.field)
    const dispatch = useDispatch()

    return <div className={scss.controlPanel}>
        <div className={scss.filterPanel}>
            <div className={scss.title}>Сортировка</div>
            <label htmlFor="ascending" onClick={() => dispatch(sortAscendingFields())}>
                <input
                    id="ascending"
                    name="ascending"
                    type='radio'
                    checked={sorting.ascendingFields}
                    readOnly={true}
                    />
                <div>по возрастанию</div>
            </label>
            <label htmlFor="descending" onClick={() => dispatch(sortDescendingFields())}>
                <input
                    id="descending"
                    name="descending"
                    type='radio'
                    readOnly={true}
                    checked={sorting.descendingFields}/>
                <div>по убыванию</div>
            </label>
        </div>
        <div className={scss.filterPanel}>
            <div className={scss.title}>Фильтр</div>
            {fluidTypes.map(type => {
                return <label htmlFor={type.id} key={type.id} onClick={() => dispatch(sortFluidType({type: type.name}))}>
                    <input
                        id={type.id}
                        name={type.name}
                        type={'radio'}
                        readOnly={true}
                        checked={sorting.sortFluidType === type.name}/>
                    <div>{type.name}</div>
                </label>
            })}
        </div>
        <Button content='Сбросить' theme='light' onClick={() => dispatch(resetSorting())}/>
    </div>
}

export default ControlPanel;