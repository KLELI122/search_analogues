import scss from './GroupDeposit.module.scss'
import ArrowLeft from "../Icons/ArrowLeft";
import {useState} from "react";
import {returnDepositProperties} from "../../utils/objectProperties.jsx";

const GroupDeposit = ({content, isDeleted}) => {

    const [isOpenGroup, setIsOpenGroup] = useState(false);

    return <div className={scss.groupDeposit}>
        <div
            className={`${scss.title} ${isOpenGroup && scss.active}`}
            onClick={() => {
                setIsOpenGroup(!isOpenGroup)
            }}>
            <ArrowLeft/>
            <div>{content.name}</div>
        </div>
        <div className={scss.contentGroup}>
            {isOpenGroup && returnDepositProperties(content).map(item => {
                return <div key={item.title}>
                    <div className={scss.content}>
                        <div>{item.title}</div>
                        <div>{item.content}</div>
                    </div>
                </div>
            })}
        </div>
    </div>
}
export default GroupDeposit;