import scss from'./SearchPage.module.scss'
import SubsoilMap from "../SubsoilMap/SubsoilMap";
import {setActiveFieldMap, setActiveDepositMap, setActiveFluidTypeMap, setActiveCollectorTypeMap, resetActiveField} from "../../store/fieldSlice";
import {useDispatch, useSelector} from "react-redux";
import CustomSelect from "../CustomSelect/CustomSelect";
import {setActiveObject, resetActiveObject} from "../../store/mapSlice";
import {setSearchParameter, fetchGetSubsoilObjectsBySubject, resetSearchParameter, fetchPostSearchAnalogues} from "../../store/searchSlice";
import Button from "../Button/Button";
import Loading from "../Icons/Loading";
import {useEffect, useState} from "react";
import ResulPanel from "../ResultPanel/ResulPanel";
import {returnDepositProperties} from '../../utils/objectProperties.jsx'

const SearchPage = () => {
    const dispatch = useDispatch();

    const {userFields, fields, activeFieldMap, fluidTypes, activeFluidTypeMap, activeDepositMap, collectorTypes, activeCollectorTypeMap} = useSelector(state => state.field);
    const {isLoading, subsoilObjects, activeSubsoilObject} = useSelector(state => state.map);
    const {searchParameters, activeSearchParameter, analogues} = useSelector(state => state.search);
    const {userInfo} = useSelector(state => state.user);

    const onChangeField = (field) => {
        dispatch(setActiveObject(subsoilObjects.find(object => object.id === field.subsoilObjectId)))
        dispatch(setActiveFieldMap(field))
        dispatch(resetSearchParameter())
    }

    const onChangeSearchParameter = (searchParameter) => {
        dispatch(setSearchParameter(searchParameter))
        if (activeSubsoilObject && searchParameter.name === 'Поиск в пределах субъекта') {
            dispatch(fetchGetSubsoilObjectsBySubject({id: activeSubsoilObject.subjectId, token: userInfo.token}))
        }
    }

    const onClickReset = () => {
        dispatch(resetActiveField())
        dispatch(resetActiveObject())
        dispatch(resetSearchParameter())
    }

    const [isDisabledBtnSearch, setIsDisabledBtnSearch] = useState(true)

    useEffect(() => {
        activeFieldMap && activeDepositMap ? setIsDisabledBtnSearch(false) : setIsDisabledBtnSearch(true)
    }, [activeFieldMap, activeDepositMap]);

    const onClickStartSearch = () => {
        if (!isDisabledBtnSearch) {
            dispatch(fetchPostSearchAnalogues({
                field: activeFieldMap.id,
                fluidType: activeFluidTypeMap.id,
                deposit: activeDepositMap,
                collectorType: activeCollectorTypeMap.id,
                searchParameter: activeSearchParameter.name,
                subject: activeSubsoilObject.subjectId,
                userId: userInfo.id,
                token: userInfo.token,
            }))
        }
    }

    return <main>
        {isLoading && <div className={scss.loading}>
            <Loading/>
            <span>Загрузка данных...</span>
        </div>}
        <SubsoilMap/>
        <div className={scss.searchPanel}>
            <CustomSelect
                title='Месторождение'
                defOptions={[...fields, ...userFields]}
                currentValue={activeFieldMap}
                onChange={onChangeField}/>
            <CustomSelect
                title='Тип флюида'
                defOptions={fluidTypes}
                currentValue={activeFluidTypeMap || ''}
                onChange={(fluidType) => {dispatch(setActiveFluidTypeMap(fluidType))}}/>
            <CustomSelect
                title='Пласт'
                defOptions={activeFieldMap.deposits || []}
                currentValue={activeDepositMap ? activeDepositMap : ''}
                onChange={(deposit) => dispatch(setActiveDepositMap(deposit))}/>
            <CustomSelect
                title='Тип коллектора'
                defOptions={collectorTypes}
                currentValue={activeCollectorTypeMap || ''}
                onChange={(collectorType) => {dispatch(setActiveCollectorTypeMap(collectorType))}}/>
            <div className={scss.propertiesPanel}>
                <div className={scss.title}>Параметры пласта</div>
                {returnDepositProperties(activeDepositMap).map(item => {
                    if (item.title !== 'Тип коллектора') {
                        return  <div key={item.title}>
                            <div className={scss.content}>
                                <div>{item.title}</div>
                                <div>{item.content}</div>
                            </div>
                        </div>
                    }})}
            </div>
            <CustomSelect
                title='Параметры поиска'
                defOptions={searchParameters}
                currentValue={activeSearchParameter}
                onChange={onChangeSearchParameter}/>
            <div className={scss.btnPanel}>
                <Button
                    content='Сбросить'
                    onClick={onClickReset}
                    theme='dark'/>
                <Button
                    content='Начать поиск'
                    isDisabled={isDisabledBtnSearch}
                    onClick={onClickStartSearch}
                    theme='dark'/>
                <Button
                    content='Результаты поиска'
                    isDisabled={!analogues.length}
                    theme='dark'
                    onClick={() => window.scrollTo({
                        top: window.innerHeight,
                        behavior: "smooth",
                    })}/>
            </div>
        </div>
        {analogues.length && <ResulPanel />}
    </main>
}

export default SearchPage;