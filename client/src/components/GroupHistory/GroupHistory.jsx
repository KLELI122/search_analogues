import scss from './GroupHistory.module.scss';
import {useState} from "react";
import ArrowLeft from "../Icons/ArrowLeft";
import {analogueItems, table} from "../../utils/objectProperties.jsx";

const GroupHistory = ({content}) => {
    const [isOpen, setIsOpen] = useState(false);

    const date = content.date.split('T')[0]
    return <div className={scss.searchItem}>
        <div className={`${scss.header} ${isOpen && scss.active}`} onClick={() => {setIsOpen(!isOpen)}}>
            <ArrowLeft />
            <div>{date.split('-')[2]}.{date.split('-')[1]}.{date.split('-')[0]}</div>
            <div>{content.field}</div>
        </div>
        {isOpen && <div className={scss.content}>
                <table>
                    <thead>
                    <tr>
                        {table.map(item => {
                            return <th scope="col" key={item}>{item}</th>
                        })}
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        {analogueItems(content).map(item => {
                            return <th scope="row" key={item.title}>{item.content}</th>
                        })}
                    </tr>
                    {content.analogues.map(item => {
                        return <tr key={item.id}>
                            {analogueItems(item).map(item => {
                                return <th scope="row" key={item.title}>{item.content}</th>
                            })}
                        </tr>
                    })}
                    </tbody>
                </table>
        </div>}
    </div>;
}

export default GroupHistory;