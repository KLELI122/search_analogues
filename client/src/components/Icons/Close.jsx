import React from 'react';

const Close = () => {
    return <svg width="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">

        <g id="SVGRepo_bgCarrier" strokeWidth="0"/>

        <g id="SVGRepo_tracerCarrier" strokeLinecap="round" strokeLinejoin="round"/>

        <g id="SVGRepo_iconCarrier">
            <g id="Menu / Close_LG">
                <path id="Vector" d="M21 21L12 12M12 12L3 3M12 12L21.0001 3M12 12L3 21.0001" stroke="#808080"
                      strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
            </g>
        </g>

    </svg>
}

export default Close;
