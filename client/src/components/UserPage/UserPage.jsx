import scss from './UserPage.module.scss'
import {useDispatch, useSelector} from "react-redux";
import Button from "../Button/Button";
import {useEffect, useState} from "react";
import {exit} from "../../store/userSlice";
import {useNavigate} from "react-router";
import {fetchGetHistorySearch} from "../../store/searchSlice";
import PasswordPanel from "../PasswordPanel/PasswordPanel";
import GroupHistory from "../GroupHistory/GroupHistory";

const UserPage = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate();

    const {userInfo} = useSelector(state => state.user);
    const {historySearch} = useSelector(state => state.search);
    const [isOpenPasswordPanel, setIsOpenPasswordPanel] = useState(false)

    useEffect(() => {
        dispatch(fetchGetHistorySearch({id: userInfo.id, token: userInfo.token}))
    }, []);

    return <main>
        <div className={scss.userPanel}>
            <div className={scss.header}>
                <div className={scss.user}>
                    <div>Пользователь:</div>
                    <div>{userInfo.username}</div>
                </div>
                <div className={scss.btnPanel}>
                    <Button
                        content='Сменить пароль'
                        onClick={() => setIsOpenPasswordPanel(true)}
                        theme='dark'/>
                    <Button
                        content='Выход'
                        onClick={() => {
                            dispatch(exit())
                            navigate('/login')
                        }}
                        theme='dark'/>
                </div>
            </div>
            <div className={scss.title}>История поиска</div>
            {historySearch.length > 0 ? <div className={scss.historySearch}>
                {historySearch.map(item => {
                    return <GroupHistory content={item} key={item.searchId}/>
                })}
            </div> : <div>Здесь будет отображаться ваша история поиска</div>}
            {isOpenPasswordPanel && <PasswordPanel setIsOpen={setIsOpenPasswordPanel}/>}
        </div>
    </main>
}

export default UserPage;