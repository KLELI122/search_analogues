import scss from './Sidebar.module.scss'
import {Link} from "react-router-dom";
import logo from '../../assets/logo.svg'
import ArrowLeft from '../Icons/ArrowLeft'
import {useLocation} from "react-router-dom";
import {useState} from "react";
import {useSelector} from "react-redux";
import {sidebar} from '../../utils/objectProperties.jsx'

const Sidebar = () => {
    const {userInfo} = useSelector((state) => state.user);
    const [isOpen, setIsOpen] = useState(true);
    const location = useLocation();

    const setCloseClass = (defaultClass) => {
        return `${defaultClass} ${!isOpen && scss.closed}`
    }

    return <div className={setCloseClass(scss.sidebar)}>
        <Link to={'/'} className={setCloseClass(scss.logo)}>
            <img src={logo} alt='logo'/>
            <div>Search Analogues</div>
        </Link>
        <nav className={scss.navigation}>
            {sidebar(userInfo.id).map(item => {
                return <Link
                    key={item.link}
                    to={item.link}
                    className={
                        `${setCloseClass(scss.navigationItem)} 
                    ${location.pathname.includes(item.link) && scss.located}`}>
                    {item.icon}
                    <div>{item.title}</div>
                </Link>
            })}
        </nav>
        <button className={setCloseClass(scss.closeBtn)}
                onClick={() => setIsOpen(!isOpen)}>
            <div>свернуть</div>
            <ArrowLeft/>
        </button>
    </div>
}

export default Sidebar;
