import scss from'./GroupField.module.scss'
import ArrowLeft from '../Icons/ArrowLeft'
import {useState} from "react";
import GroupDeposit from "../GroupDeposit/GroupDeposit";
import Button from "../Button/Button";
import UpdateDepositPanel from "../UpdateDepositPanel/UpdateDepositPanel";

const GroupField = ({content}) => {
    const [isOpenGroup, setIsOpenGroup] = useState(false);
    const [isOpenDepositPanel, setIsOpenDepositPanel] = useState(false);

    return <div className={scss.group}>
        <div className={`${scss.title} ${isOpenGroup && scss.active}`}
             onClick={() => setIsOpenGroup(!isOpenGroup)}>
            <ArrowLeft />
            <div>{content.name}</div>
        </div>

        {isOpenGroup &&
            <div className={scss.groupContent}>
                <div className={scss.content}>
                    <div>Объект недропользования:</div>
                    <div>{content.subsoilObject}</div>
                </div>
                <div className={scss.content}>
                    <div>Тип флюида:</div>
                    <div>{content.fluidType}</div>
                </div>
                <div className={scss.content}>
                    <div>Пласты:</div>
                </div>
                {content.deposits.map(item => {
                    return <GroupDeposit key={item.id} content={item} isDeleted={!!content.userId} />
                })}
                {content.userId && <Button
                    content={'+ Добавить пласт'}
                    onClick={() => setIsOpenDepositPanel(true)}
                    theme='light'/>}
            </div>}
        {isOpenDepositPanel && <UpdateDepositPanel
            setIsOpen={setIsOpenDepositPanel}
            content={[]}
            currentField={content}/>}
    </div>
}

export default GroupField;