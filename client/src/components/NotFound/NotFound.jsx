import scss from './NotFound.module.scss'
import {Link} from "react-router-dom";

const NotFound = () => {
    return <main className={scss.notFoundPage}>
        <div className={scss.notFoundInfo}>Страница не найдена...</div>
        <Link to={'/'}>
            ←  на главную
        </Link>
    </main>
}

export default NotFound;