import scss from './Tab.module.scss'

const Tab = ({label, isOpen, onClick}) => {

    return <div
        className={`${scss.tab} ${isOpen && scss.open}`}
        onClick={onClick}>
        {label}
    </div>
}

export default Tab;