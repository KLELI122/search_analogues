import scss from '../LoginPage/LoginPage.module.scss'
import {useState, useEffect} from "react";
import Input from "../Input/Input";
import Button from "../Button/Button";
import {fetchPostRegister} from "../../store/userSlice";
import {useDispatch} from "react-redux";

const SignUpPanel = () => {

    const [login, setLogin] = useState('')
    const [isErrorSignUpLogin, setIsErrorSignUpLogin] = useState('')

    const [password, setPassword] = useState('')
    const [isErrorSignUpPassword, setIsErrorSignUpPassword] = useState('')

    const changeValueSignUp = (e) => {
        const inputValue = e.target.value
        switch (e.target.name) {
            case 'signUpLogin':
                inputValue === '' ? setIsErrorSignUpLogin('Заполните поле') : setIsErrorSignUpLogin('')
                setLogin(inputValue)
                break;
            case 'signUpPassword':
                inputValue.length < 8 ? setIsErrorSignUpPassword('Пароль должен содержать не менее 8 символов') : setIsErrorSignUpPassword('')
                setPassword(inputValue)
                break;
        }
    }

    const [isDisabledBtn, setIsDisabledBtn] = useState(true)

    useEffect(() => {
        login && password && setIsDisabledBtn(isErrorSignUpLogin !== '' || isErrorSignUpPassword !== '')
    }, [login, password, isErrorSignUpLogin, isErrorSignUpPassword]);


    const dispatch = useDispatch()

    const sendSignUp = (e) => {
        e.preventDefault()
        !isDisabledBtn && dispatch(fetchPostRegister({
            username: login,
            password: password
        }))

    }

    return <form onSubmit={(e) => {sendSignUp(e)}}>
        <div className={scss.signUpPanel}>
            <Input
                labelContent='Логин:'
                inputName='signUpLogin'
                inputType='text'
                changeValueState={changeValueSignUp}
                isError={isErrorSignUpLogin}/>
            <Input
                labelContent='Пароль:'
                inputName='signUpPassword'
                inputType='password'
                changeValueState={changeValueSignUp}
                isError={isErrorSignUpPassword}/>
        </div>
        <Button
            content='Зарегестироваться'
            isDisabled={isDisabledBtn}
            theme='dark'/>
    </form>
}

export default SignUpPanel;