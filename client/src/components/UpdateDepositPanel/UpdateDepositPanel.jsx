import scss from './UpdateDepositPanel.module.scss'
import Input from "../Input/Input";
import CustomSelect from "../CustomSelect/CustomSelect";
import Button from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {returnDepositProp} from "../../utils/objectProperties.jsx";
import {fetchGetUserFields, fetchPostAddDeposit} from "../../store/fieldSlice";

const UpdateDepositPanel = ({setIsOpen, content, currentField}) => {
    const dispatch = useDispatch();
    const {userInfo} = useSelector(state => state.user)
    const {collectorTypes} = useSelector(state => state.field);
    const [isDisabledBtn, setIsDisabledBtn] = useState(true)

    const [depositName, setDepositName] = useState('');
    const [collectorType, setCollectorType] = useState('');
    const [recoveryFactor, setRecoveryFactor] = useState(null);
    const [porosity, setPorosity] = useState(null);
    const [permeability, setPermeability] = useState(null);
    const [saturation, setSaturation] = useState(null);
    const [density, setDensity] = useState(null);
    const [viscosity, setViscosity] = useState(null);

    const changeDepositProp = (prop, value) => {
        switch (prop) {
            case 'Коэф. извлечения нефти': setRecoveryFactor(value)
                break;
            case 'Пористость': setPorosity(value)
                break;
            case 'Проницаемость': setPermeability(value)
                break;
            case 'Нефтенасыщенность': setSaturation(value)
                break;
            case 'Плотность': setDensity(value)
                break;
            case 'Вязкость': setViscosity(value)
                break;
        }
    }

    const sendNewDeposit = async () => {
        if (!isDisabledBtn) {
            await dispatch(fetchPostAddDeposit({
                token: userInfo.token,
                fieldId: currentField.id,
                name: depositName,
                collectorTypeId: collectorType.id,
                recoveryFactor: recoveryFactor,
                porosity: porosity,
                permeability: permeability,
                saturation: saturation,
                density: density,
                viscosity: viscosity
            }))
            setIsOpen(false);
            dispatch(fetchGetUserFields({id: userInfo.id, token: userInfo.token}))
        }
    }

    useEffect(() => {
        depositName && collectorType ? setIsDisabledBtn(false) :  setIsDisabledBtn(true)
    }, [depositName, collectorType]);

    return <div className={scss.addDeposit}>
        <div>
            <Input
                inputName='nameDeposit'
                inputType='text'
                placeholder={'Название пласта'}
                changeValueState={(e) => {setDepositName(e.target.value)}}
                hideError={true}/>
            <CustomSelect
                placeholder={'Тип коллектора'}
                defOptions={collectorTypes}
                onChange={(type) => {setCollectorType(type)}}/>
            {returnDepositProp(content).map(item => {
                return <Input
                    key={item.title}
                    inputName={item.title}
                    inputType='text'
                    placeholder={item.title}
                    changeValueState={(e) => {changeDepositProp(item.title, e.target.value)}}
                    hideError={true}/>
            })}
        </div>
        <div className={scss.btnPanel}>
            <Button
                content='Сохранить'
                theme={'light'}
                onClick={sendNewDeposit}
                isDisabled={isDisabledBtn}/>
            <Button
                content='Отменить'
                theme={'light'}
                onClick={() => {
                    setIsOpen(false)
                }}/>
        </div>
    </div>
}

export default UpdateDepositPanel;