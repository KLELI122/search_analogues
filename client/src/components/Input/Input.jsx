import scss from './Input.module.scss'
import {useState} from "react";

const Input = ({labelContent, inputName, inputType, placeholder, changeValueState, isError, hideError, currentValue}) => {

    const [value, setValue] = useState(currentValue && currentValue.name || '')

    const onChange = (e) => {
        changeValueState(e)
        setValue(e.target.value)
    }

    return <div className={scss.inputPanel}>
        {labelContent && <label>{labelContent}</label>}
        <input
            id={inputName}
            name={inputName}
            type={inputType}
            placeholder={placeholder && placeholder}
            className={`${isError && scss.error}`}
            value={value}
            onBlur={(e) => onChange(e)}
            onChange={(e) => onChange(e)}/>
        {!hideError && <div className={scss.info}>
            {isError && isError}
        </div>}
    </div>
}

export default Input;