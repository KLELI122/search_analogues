import scss from './Button.module.scss'

const Button = ({content, isDisabled, onClick, theme}) => {
    return <button
        className={`
            ${scss.btn} 
            ${isDisabled && scss.disabled} 
            ${theme === 'light' && scss.light}
            ${theme === 'dark' && scss.dark}`}
        onClick={onClick}>
        {content}
    </button>
}

export default Button;
