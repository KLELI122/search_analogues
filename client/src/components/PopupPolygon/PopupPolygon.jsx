import scss from './PopupPolygon.module.scss'
import {Popup} from "react-leaflet";
import {returnSubsoilProperties} from "../../utils/objectProperties.jsx";

const PopupPolygon = ({object}) => {
    return <Popup>
        <div className={scss.subsoilObjectPopup}>
            <div className={scss.title}>Объект недропользования:</div>
            <a href={object.hyperLink.split('"')[3]} target='_blank' className={scss.content}>
                <div>{object.name}</div>
            </a>
        </div>
        {returnSubsoilProperties(object).map(item => {
            return <div key={item.title} className={scss.subsoilObjectPopup}>
                <div className={scss.title}>{item.title}:</div>
                <div className={scss.content}>{item.content}</div>
            </div>
        })}
    </Popup>
}

export default PopupPolygon;