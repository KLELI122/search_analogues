import slide1 from '../assets/slide1.png'
import slide2 from '../assets/slide2.png'
import slide3 from '../assets/slide3.png'
import ProfileIcon from "../components/Icons/Profile";
import SearchIcon from "../components/Icons/Search";
import FieldsIcon from "../components/Icons/Fields";

export const slider = [
    {index: 0, title: 'Поиск аналогов', img: slide1},
    {index: 1, title: 'База месторождений', img: slide2},
    {index: 2, title: 'История поиска', img: slide3},
]

export const sidebar = (id) => {
    return [
        { title: 'Личный кабинет', link: `/login/${id}`, icon: <ProfileIcon /> },
        { title: 'Поиск аналогов', link: '/search', icon: <SearchIcon /> },
        { title: 'Месторождения', link: '/fields', icon: <FieldsIcon /> },
    ]
}

export const table = [
    'Месторождение', 'Тип флюида', 'Пласт', 'Тип коллектора', 'Коэф. извлечения нефти', 'Пористость',  'Проницаемость', 'Нефтенасыщенность', 'Плотность', 'Вязкость'
]

export const analogueItems = (object) => {
    return [
        {title: 'Месторождение', content: object.field},
        {title: 'Тип флюида', content: object.fluidType},
        {title: 'Пласт', content: object.name},
        {title: 'Тип коллектора', content: object.collectorType},
        {title: 'Коэф. извлечения нефти', content: object.recoveryFactor},
        {title: 'Пористость', content: object.porosity},
        {title: 'Проницаемость', content: object.permeability},
        {title: 'Нефтенасыщенность', content: object.saturation},
        {title: 'Плотность', content: object.density},
        {title: 'Вязкость', content: object.viscosity},
    ]
}

export const returnSubsoilProperties = (object) => {
    return [
        { title: 'Федеральный округ', content: object.federalDistrict },
        { title: 'Субъект федерации', content: object.subjectFederation },
        { title: 'Регистрационный номер', content: object.licenseNumber },
        { title: 'Дата регистрации', content: object.registrationDate },
        { title: 'Дата окончания регистрации', content: object.expirationDate },
        { title: 'Тип пользования', content: object.useType },
        { title: 'Вид полезного ископаемого', content: object.mineralResource },
        { title: 'Пользователь недр', content: object.subsoilUser },
    ]
}

export const returnDepositProperties = (object) => {
    return [
        {title: 'Тип коллектора', content: object.collectorType},
        {title: 'Коэф. извлечения нефти', content: object.recoveryFactor},
        {title: 'Пористость', content: object.porosity},
        {title: 'Проницаемость', content: object.permeability},
        {title: 'Нефтенасыщенность', content: object.saturation},
        {title: 'Плотность', content: object.density},
        {title: 'Вязкость', content: object.viscosity},
    ]
}

export const returnDepositProp = (object) => {
    return [
        {title: 'Коэф. извлечения нефти', content: object.recoveryFactor},
        {title: 'Пористость', content: object.porosity},
        {title: 'Проницаемость', content: object.permeability},
        {title: 'Нефтенасыщенность', content: object.saturation},
        {title: 'Плотность', content: object.density},
        {title: 'Вязкость', content: object.viscosity},
    ]
}