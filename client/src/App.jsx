import {BrowserRouter, Route, Routes} from "react-router-dom";
import Sidebar from "./components/Sidebar/Sidebar";
import HomePage from "./components/HomePage/HomePage";
import LoginPage from "./components/LoginPage/LoginPage";
import UserPage from "./components/UserPage/UserPage";
import SearchPage from "./components/SearchPage/SearchPage";
import FieldsPage from "./components/FieldsPage/FieldsPage";
import NotFound from "./components/NotFound/NotFound";
import {fetchGetSubsoilObjects,} from "./store/mapSlice";
import {fetchGetFields, fetchGetUserFields} from "./store/fieldSlice";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";

const App = () => {
    const dispatch = useDispatch();
    const {userInfo} = useSelector(state => state.user);
    const {fields} = useSelector(state => state.field);

    useEffect(() => {
        if (userInfo.token) {
            !fields.length && dispatch(fetchGetFields(userInfo.token))
            dispatch(fetchGetUserFields({id: userInfo.id, token: userInfo.token}))
            dispatch(fetchGetSubsoilObjects(userInfo.token))
        }
    }, [userInfo]);

    return <BrowserRouter>
        {userInfo.token && <Sidebar/>}
        <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/login" element={<LoginPage/>}/>
            {userInfo.token && <>
                <Route path="/login/:id" element={<UserPage />} />
                <Route path="/search" element={<SearchPage />} />
                <Route path="/fields" element={<FieldsPage />} />
            </>}
            <Route path="*" element={<NotFound />} />
        </Routes>
    </BrowserRouter>
}

export default App;
