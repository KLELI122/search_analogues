const convert = (locationArea = '') => {
    if (locationArea && (locationArea?.startsWith('MULTIPOLYGON') || locationArea?.startsWith('POLYGON'))) {
        const polygonsStr = locationArea.replace(/, /g, ',').replace(/ \(/g, '(');
        const polygons = polygonsStr.split(')),((');
        polygons[0] = polygons[0].replace('MULTIPOLYGON(((', '');
        polygons[0] = polygons[0].replace('POLYGON((', '');

        const lastIndex = polygons.length - 1;
        polygons[lastIndex] = polygons[lastIndex].replace(')))', '');
        polygons[lastIndex] = polygons[lastIndex].replace('))', '');

        for (let i = 0; i < polygons.length; i++) {
            polygons[i] = polygons[i]
                .split('),(')
                .map((points) => points.split(','))
                .map((points) => points.map((point) => point.split(' ').reverse()));
        }
        return polygons;
    }
    return [];
};

module.exports = convert