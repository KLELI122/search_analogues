const express = require('express');
const app = express();
const cors = require('cors');

app.use(express.json());
app.use(cors())

const MapService = require('./services/mapService');
const MapController = require('./controllers/mapController');

const mapRoutes = require('./routes/mapRoutes');
const mapModel = require('./models/mapModel');

const mapService = new MapService(mapModel);
const mapController = new MapController(mapService);

app.use('/api', mapRoutes(mapController));

const FieldService = require('./services/fieldService');
const FieldController = require('./controllers/fieldController');

const fieldRoutes = require('./routes/fieldRoutes');
const fieldModel = require('./models/fieldModel');

const fieldService = new FieldService(fieldModel);
const fieldController = new FieldController(fieldService);

app.use('/api', fieldRoutes(fieldController));

const SearchService = require('./services/searchService');
const SearchController = require('./controllers/searchController');

const searchRoutes = require('./routes/searchRoutes');
const searchModel = require('./models/searchModel');

const searchService = new SearchService(searchModel);
const searchController = new SearchController(searchService);

app.use('/api', searchRoutes(searchController));

const UserService = require('./services/userService');
const UserController = require('./controllers/userController');

const userRoutes = require('./routes/userRoutes');
const userModel = require('./models/userModel');

const userService = new UserService(userModel);
const userController = new UserController(userService);

app.use('/api', userRoutes(userController));

app.get('/', (req, res) => {
    res.status(200).type('text/plain');
    res.send('About page');
});

const PORT = 5000
app.listen(PORT, () => {
    console.log(`Server listen http://localhost:${PORT}`);
});
