const express = require('express');
const authenticateJWT = require('../middleware/authenticateJWT');

module.exports = (mapController) => {
    const router = express.Router();
    router.get('/subsoils', authenticateJWT, mapController.getSubsoilObjects)
    router.get('/subsoils/:id', authenticateJWT, mapController.getIdSubsoilObjectByFieldId)
    return router;
}