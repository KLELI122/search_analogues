const express = require('express');
const authenticateJWT = require('../middleware/authenticateJWT');

module.exports = (userController) => {
    const router = express.Router();
    router.post('/register', userController.registrationUser)
    router.post('/login', userController.authenticationUser)
    router.post('/updatePassword', authenticateJWT, userController.changePassword)
    return router;
}