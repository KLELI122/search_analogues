const express = require('express');
const authenticateJWT = require("../middleware/authenticateJWT");

module.exports = (searchController) => {
    const router = express.Router();
    router.get('/subsoilsBySubject/:id', authenticateJWT, searchController.getSubsoilObjectsBySubject)
    router.post('/search', authenticateJWT, searchController.searchAnalogues)
    router.get('/history/:id', authenticateJWT, searchController.getSearchHistory)
    return router;
}