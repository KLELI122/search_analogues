const express = require('express');
const authenticateJWT = require('../middleware/authenticateJWT');

module.exports = (fieldController) => {
    const router = express.Router();
    router.get('/fields/', authenticateJWT, fieldController.getFields)
    router.get('/fields/:id', authenticateJWT, fieldController.getUserFields)
    router.post('/addField', authenticateJWT, fieldController.addField)
    router.post('/updateField', authenticateJWT, fieldController.updateField)
    router.delete('/deleteField/:id', authenticateJWT, fieldController.deleteField)
    router.post('/addDeposit', authenticateJWT, fieldController.addDeposit)
    router.get('/field/', fieldController.getFields)
    return router;
}