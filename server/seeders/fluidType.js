/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */

exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('fluid_type').del()
  await knex('fluid_type').insert([
    {id: 1, name: 'Нефтяное'},
    {id: 2, name: 'Газонефтяное'},
    {id: 3, name: 'Нефтегазоконденсатное'},
    {id: 4, name: 'Нефтегазовое'},
  ]);
};
