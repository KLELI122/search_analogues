/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */

exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('federal_district').del()
  await knex('federal_district').insert([
    {id: 1, name: 'Дальневосточный'},
    {id: 2, name: 'Приволжский'},
    {id: 3, name: 'Северо-Западный'},
    {id: 4, name: 'Северо-Кавказский'},
    {id: 5, name: 'Сибирский'},
    {id: 6, name: 'Уральский'},
    {id: 7, name: 'Шельф Российской Федерации'},
    {id: 8, name: 'Южный'},
    {id: 9, name: 'Крымский'},
  ]);
};
