/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('collector_type').del()
  await knex('collector_type').insert([
    {id: 1, name: 'терригенный'},
    {id: 2, name: 'карбонатный'},
  ]);
};
