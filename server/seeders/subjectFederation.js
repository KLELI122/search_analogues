/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */

exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('subject_federation').del()
  await knex('subject_federation').insert([
    {id: 1, name: 'Амурская область', fk_federal_district: 1},
    {id: 2, name: 'Астраханская область', fk_federal_district: 8},
    {id: 3, name: 'Волгоградская область', fk_federal_district: 8},
    {id: 4, name: 'Иркутская область', fk_federal_district: 5},
    {id: 5, name: 'Кабардино-Балкарская Республика', fk_federal_district: 4},
    {id: 6, name: 'Калининградская область', fk_federal_district: 3},
    {id: 7, name: 'Камчатский край', fk_federal_district: 1},
    {id: 8, name: 'Кемеровская область', fk_federal_district: 5},
    {id: 9, name: 'Кировская область', fk_federal_district: 2},
    {id: 10, name: 'Краснодарский край', fk_federal_district: 8},

    {id: 11, name: 'Красноярский край', fk_federal_district: 5},
    {id: 12, name: 'Курганская область', fk_federal_district: 6},
    {id: 13, name: 'Ненецкий АО', fk_federal_district: 3},
    {id: 14, name: 'Новосибирская область', fk_federal_district: 5},
    {id: 15, name: 'Омская область', fk_federal_district: 5},
    {id: 16, name: 'Оренбургская область', fk_federal_district: 2},
    {id: 17, name: 'Пензенская область', fk_federal_district: 2},
    {id: 18, name: 'Пермский край', fk_federal_district: 2},
    {id: 19, name: 'Приморский край', fk_federal_district: 1},
    {id: 20, name: 'Республика Адыгея', fk_federal_district: 8},

    {id: 21, name: 'Республика Башкортостан', fk_federal_district: 2},
    {id: 22, name: 'Республика Бурятия', fk_federal_district: 1},
    {id: 23, name: 'Республика Дагестан', fk_federal_district: 4},
    {id: 24, name: 'Республика Ингушетия', fk_federal_district: 4},
    {id: 25, name: 'Республика Калмыкия', fk_federal_district: 8},
    {id: 26, name: 'Республика Карелия', fk_federal_district: 3},
    {id: 27, name: 'Республика Коми', fk_federal_district: 3},
    {id: 28, name: 'Республика Крым', fk_federal_district: 9},
    {id: 29, name: 'Республика Саха (Якутия)', fk_federal_district: 1},
    {id: 30, name: 'Республика Северная Осетия - Алания', fk_federal_district: 4},

    {id: 31, name: 'Республика Татарстан', fk_federal_district: 2},
    {id: 32, name: 'Республика Хакасия', fk_federal_district: 5},
    {id: 33, name: 'Ростовская область', fk_federal_district: 8},
    {id: 34, name: 'Самарская область', fk_federal_district: 2},
    {id: 35, name: 'Саратовская область', fk_federal_district: 2},
    {id: 36, name: 'Сахалинская область', fk_federal_district: 1},
    {id: 37, name: 'Свердловская область', fk_federal_district: 6},
    {id: 38, name: 'Ставропольский край', fk_federal_district: 4},
    {id: 39, name: 'Томская область', fk_federal_district: 5},
    {id: 40, name: 'Тюменская область', fk_federal_district: 6},
    {id: 41, name: 'Удмуртская Республика', fk_federal_district: 2},

    {id: 42, name: 'Ульяновская область', fk_federal_district: 2},
    {id: 43, name: 'Хабаровский край', fk_federal_district: 2},
    {id: 44, name: 'Ханты-Мансийский АО', fk_federal_district: 6},
    {id: 45, name: 'Чеченская Республика', fk_federal_district: 4},
    {id: 46, name: 'Чукотский АО', fk_federal_district: 1},
    {id: 47, name: 'Шельф Азовского моря', fk_federal_district: 7},
    {id: 48, name: 'Шельф Балтийского моря', fk_federal_district: 7},
    {id: 49, name: 'Шельф Баренцева моря', fk_federal_district: 7},
    {id: 50, name: 'Шельф Восточно-Сибирского моря', fk_federal_district: 7},
    {id: 51, name: 'Шельф Карского моря', fk_federal_district: 7},

    {id: 52, name: 'Шельф Каспийского моря', fk_federal_district: 7},
    {id: 53, name: 'Шельф моря Лаптевых', fk_federal_district: 7},
    {id: 54, name: 'Шельф Охотского моря', fk_federal_district: 7},
    {id: 55, name: 'Шельф Печорского моря', fk_federal_district: 7},
    {id: 56, name: 'Шельф Чёрного моря', fk_federal_district: 7},
    {id: 57, name: 'Шельф Чукотского моря', fk_federal_district: 7},
    {id: 58, name: 'Шельф Японского моря', fk_federal_district: 7},
    {id: 59, name: 'Ямало-Ненецкий АО', fk_federal_district: 6},
  ]);
};
