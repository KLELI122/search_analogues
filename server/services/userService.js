class UserService {

    constructor(userModel) {
        this.userModel = userModel;
    }

    async registrationUser(username, hashedPassword) {
        return this.userModel.registrationUser(username, hashedPassword);
    }

    async authenticationUser(username) {
        return this.userModel.authenticationUser(username);
    }

    async userById(id) {
        return this.userModel.userById(id);
    }

    async changePassword(id, hashPassword) {
        return this.userModel.changePassword(id, hashPassword);
    }
}

module.exports = UserService;
