class MapService {

    constructor(mapModel) {
        this.mapModel = mapModel;
    }

    async getSubsoilObjects() {
        return this.mapModel.getSubsoilObjects();
    }

    async getIdSubsoilObjectByFieldId(id) {
        return this.mapModel.getIdSubsoilObjectByFieldId(id);
    }

}

module.exports = MapService;
