class SearchService {

    constructor(searchModel) {
        this.searchModel = searchModel;
    }

    async getSubsoilObjectsBySubject(id) {
        return this.searchModel.getSubsoilObjectsBySubject(id);
    }

    async searchAnalogues(field, fluidType, deposit, collectorType) {
        return this.searchModel.searchAnalogues(field, fluidType, deposit, collectorType);
    }

    async searchAnaloguesBySubject(field, fluidType, deposit, collectorType, subject) {
        return this.searchModel.searchAnaloguesBySubject(field, fluidType, deposit, collectorType, subject);
    }

    async addUserHistory(userId) {
        return this.searchModel.addUserHistory(userId);
    }

    async addSearchHistory(targetObject, analogObject, historyId) {
        return this.searchModel.addSearchHistory(targetObject, analogObject, historyId);
    }

    async getSearchTarget(userId) {
        return this.searchModel.getSearchTarget(userId);
    }

    async getSearchAnalogue(searchId) {
        return this.searchModel.getSearchAnalogue(searchId);
    }
}

module.exports = SearchService;
