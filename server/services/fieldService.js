
class FieldService {

    constructor(fieldModel) {
        this.fieldModel = fieldModel;
    }

    async getFields(id) {
        return this.fieldModel.getFields(id);
    }

    async getDeposits(id) {
        return this.fieldModel.getDeposits(id);
    }

    async addField(userId, nameField, fluidId, subsoilId) {
        return this.fieldModel.addField(userId, nameField, fluidId, subsoilId);
    }

    async updateField(userId, fieldId, nameField, fluidId, subsoilId) {
        return this.fieldModel.updateField(userId, fieldId, nameField, fluidId, subsoilId);
    }

    async deleteField(id) {
        return this.fieldModel.deleteField(id);
    }

    async addDeposit(fieldId, name, collectorTypeId, recoveryFactor, porosity, permeability, saturation, density, viscosity) {
        return this.fieldModel.addDeposit(fieldId, name, collectorTypeId, recoveryFactor, porosity, permeability, saturation, density, viscosity);
    }
}

module.exports = FieldService;
