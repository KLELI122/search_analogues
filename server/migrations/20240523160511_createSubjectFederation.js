/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.up = function(knex) {
    return knex.schema
        .createTable('subject_federation', function(table) {
            table.increments('id');
            table.string('name').notNullable();
            table
                .integer('fk_federal_district')
                .references('id')
                .inTable('federal_district')
                .notNullable();
        });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.down = function(knex) {
    return knex.schema
        .dropTable('subject_federation');
};
