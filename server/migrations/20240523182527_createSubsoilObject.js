/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.up = function(knex) {
    return knex.schema
        .createTable('subsoil_object', function(table) {
            table.increments('id');
            table.string('license_nu');
            table.string('reg_date');
            table.string('exp_date');
            table.string('use_type');
            table.string('mineral');
            table.string('user');
            table.string('name');
            table.string('hyper_link');
            table
                .integer('fk_subject')
                .references('id')
                .inTable('subject_federation');
        });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.down = function(knex) {
    return knex.schema
        .dropTable('subsoil_object');
};
