/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.up = function(knex) {
    return knex.schema
        .createTable('field', function(table) {
            table.increments('id');
            table.string('name').notNullable();
            table
                .integer('fk_fluid_type')
                .references('id')
                .inTable('fluid_type')
                .notNullable();
            table
                .integer('fk_subsoil_object')
                .references('id')
                .inTable('subsoil_object')
                .notNullable();
        });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.down = function(knex) {
    return knex.schema
        .dropTable('field');
};
