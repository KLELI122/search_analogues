/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.up = function(knex) {
    return knex.schema
        .createTable('deposit', function(table) {
            table.increments('id');
            table.string('name').notNullable();
            table.string('recovery_factor');
            table.string('porosity');
            table.string('permeability');
            table.string('saturation');
            table.string('density');
            table.string('viscosity');
            table
                .integer('fk_field')
                .references('id')
                .inTable('field')
                .notNullable();
            table
                .integer('fk_collector_type')
                .references('id')
                .inTable('collector_type')
                .notNullable();
        });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.down = function(knex) {
    return knex.schema
        .dropTable('deposit');
};
