class FieldController {

    constructor(fieldService) {
        this.fieldService = fieldService;
    }

    getFields = async (req, res) => {
        try {
            const fields = await this.fieldService.getFields(null);
            let deposits = [];
            for (const field of fields) {
                deposits.push({...field, deposits: await this.fieldService.getDeposits(field.id)});
            }
            res.status(200).json(deposits.sort((a, b) => a.name.localeCompare(b.name)));

        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }

    getUserFields = async (req, res) => {
        try {
            const fields = await this.fieldService.getFields(req.params.id);
            let deposits = [];
            for (const field of fields) {
                deposits.push({...field, deposits: await this.fieldService.getDeposits(field.id)});
            }
            res.status(200).json(deposits);

        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }

    addField = async (req, res) => {
        try {
            const {userId, nameField, fluidId, subsoilId} = req.body;
            const field = await this.fieldService.addField(userId, nameField, fluidId, subsoilId);
            return res.status(200).json(field);
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }

    updateField = async (req, res) => {
        try {
            const {userId, fieldId, nameField, fluidId, subsoilId} = req.body;
            const updateField = await this.fieldService.updateField(userId, fieldId, nameField, fluidId, subsoilId);
            return res.status(200).json(updateField);
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }

    deleteField = async (req, res) => {
        try {
            const field = await this.fieldService.deleteField(req.params.id);
            return res.status(200).json({message: 'Field deleted successfully'});
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }

    addDeposit = async (req, res) => {
        try {
            const {fieldId, name, collectorTypeId, recoveryFactor, porosity, permeability, saturation, density, viscosity} = req.body;
            const deposit = await this.fieldService.addDeposit(fieldId, name, collectorTypeId, recoveryFactor, porosity, permeability, saturation, density, viscosity);
            return res.status(200).json(deposit);
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }
}

module.exports = FieldController;
