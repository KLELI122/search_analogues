const convert = require("../utils/convertMultipolygon");

class MapController {

    constructor(mapService) {
        this.mapService = mapService;
    }

    getSubsoilObjects = async (req, res) => {

        try {
            const geometry = await this.mapService.getSubsoilObjects();
            geometry.map(object => {
                object.geometry = convert(object.geometry);
            })
            res.status(200).json(geometry);
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }

    getIdSubsoilObjectByFieldId = async (req, res) => {
        try {
            const subsoilObject = await this.mapService.getIdSubsoilObjectByFieldId(req.params.id);
            res.status(200).json(subsoilObject);
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }
}

module.exports = MapController;
