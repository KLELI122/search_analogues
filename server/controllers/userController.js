const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const SALT_ROUNDS = 10;
const SECRET_KEY = '59060141';
const SESSION_DURATION = '24h';

class UserController {

    constructor(userService) {
        this.userService = userService;
    }

    registrationUser = async (req, res) => {
        try {
            const { username, password } = req.body;
            const user = await this.userService.authenticationUser(username);
            if (user[0]) {
                return res.status(401).json({message: 'User already exists'});
            }
            const hashedPassword = await bcrypt.hash(password, SALT_ROUNDS);
            const newUser = await this.userService.registrationUser(username, hashedPassword);
            res.status(200).json(newUser);
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }

    authenticationUser = async (req, res) => {
        try {
            const { username, password } = req.body;
            const user = await this.userService.authenticationUser(username);
            if (!user[0]) {
                return res.status(401).json({ message: 'User does not exist' });
            }
            const isSameUser = await bcrypt.compare(password.toString(), user[0].password)
            if (!isSameUser) {
                return res.status(401).json({ message: 'Invalid password' });
            }
            const token = jwt.sign({ username }, SECRET_KEY, { expiresIn: SESSION_DURATION });
            return res.status(200).json({
                message: 'Authenticated',
                token,
                id: user[0].id,
                username: user[0].username
            });
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }

    changePassword = async (req, res) => {
        try {
            const { id, oldPassword, newPassword } = req.body;
            const user = await this.userService.userById(id);

            const isSameUser = await bcrypt.compare(oldPassword.toString(), user[0].password)
            if (!isSameUser) {
                return res.status(401).json({ message: 'Invalid password' });
            }
            const hashedPassword = await bcrypt.hash(newPassword, SALT_ROUNDS);

            const updateUser = await this.userService.changePassword(id, hashedPassword);
            return res.status(200).json({updateUser});

        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }
}

module.exports = UserController;
