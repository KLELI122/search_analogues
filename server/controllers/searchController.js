class SearchController {

    constructor(searchService) {
        this.searchService = searchService;
    }

    getSubsoilObjectsBySubject = async (req, res) => {
        try {
            const subsoilObjects = await this.searchService.getSubsoilObjectsBySubject(req.params.id);
            res.status(200).json(subsoilObjects);
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }

    searchAnalogues = async (req, res) => {
        try {
            const {field, fluidType, deposit, collectorType, searchParameter, subject, userId} = req.body;
            switch (searchParameter) {
                case "Поиск по всем объектам":
                    const analogues = await this.searchService.searchAnalogues(field, fluidType, deposit.name, collectorType);
                    if (analogues.length) {
                        const historyId = await this.searchService.addUserHistory(userId)
                        analogues.map(async analogue => {
                            await this.searchService.addSearchHistory(deposit.id, analogue.id, historyId[0].id)
                        })
                    }
                    res.status(200).json(analogues);
                    break;
                case "Поиск в пределах субъекта":
                    const analoguesBySubject = await this.searchService.searchAnaloguesBySubject(field, fluidType, deposit, collectorType, subject);
                    if (analoguesBySubject.length) {
                        const history = await this.searchService.addUserHistory(userId)
                        analoguesBySubject.map(async analogue => {
                            await this.searchService.addSearchHistory(deposit.id, analogue.id, history[0].id)
                        })
                    }
                    res.status(200).json(analoguesBySubject);
                    break;
            }
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }

    getSearchHistory = async (req, res) => {
        try {
            const targets = await this.searchService.getSearchTarget(req.params.id);
            let history = [];
            for (const target of targets) {
                history.push({...target, analogues: await this.searchService.getSearchAnalogue(target.searchId)});
            }
            res.status(200).json(history);
        } catch (error) {
            res.status(500).json({error: error.message});
        }
    }
}
module.exports = SearchController;
