/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */

module.exports = {
    development: {
        client: 'postgresql',
        connection: {
            database: 'deposits',
            user: 'postgres',
            password: '59060141',
            host: 'localhost',
            port: '5432',
        },
        migrations: {
            directory: 'migrations',
        },
        seeds: {
            directory: 'seeders',
        }
    }
};