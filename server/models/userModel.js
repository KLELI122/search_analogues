const {knexDbConfig} = require('../config/index');
const knex = require('knex')(knexDbConfig);
const pg = require("pg");
const {dbConfig} = require("../config");
const pool = new pg.Pool(dbConfig);

class UserModel {

    async registrationUser(username, hashPassword) {
        pool.connect()
        return knex('users')
            .insert({ username: username, password: hashPassword })
            .returning('*')
    }

    async authenticationUser(username) {
        pool.connect()
        return knex.select({
            id: 'users.id',
            username: 'users.username',
            password: 'users.password',
        })
            .from('users')
            .where('username', username)
    }

    async userById(id) {
        pool.connect()
        return knex.select('*')
            .from('users')
            .where('id', id)
    }

    async changePassword(id, hashPassword) {
        pool.connect()
        return knex('users').where('id', id).update({
            password: hashPassword,
        });
    }
}

module.exports = new UserModel();
