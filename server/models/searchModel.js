const {knexDbConfig} = require('../config/index');
const knex = require('knex')(knexDbConfig);
const pg = require("pg");
const {dbConfig} = require("../config");
const pool = new pg.Pool(dbConfig);

class SearchModel {

    async getSubsoilObjectsBySubject(id) {
        pool.connect()
        return knex.select({
            id: 'so.id',
        })
            .from({so: 'subsoil_object'})
            .leftJoin({sd: 'subject_federation'}, 'so.fk_subject', 'sd.id' )
            .whereRaw(`sd.id = ${id}`)
    }

    async searchAnalogues(field, fluidType, deposit, collectorType) {
        pool.connect()
        return knex.select({
            id: 'dp.id',
            name: 'dp.name',
            recoveryFactor: 'dp.recovery_factor',
            porosity: 'dp.porosity',
            permeability: 'dp.permeability',
            saturation: 'dp.saturation',
            density: 'dp.density',
            viscosity: 'dp.viscosity',
            collectorType: 'ct.name',
            field: 'fl.name',
            fluidType: 'ft.name',
            subsoilObjectId: 'so.id',
        })
            .from({dp: 'deposit'})
            .leftJoin({fl: 'field'}, 'dp.fk_field', 'fl.id' )
            .leftJoin({ft: 'fluid_type'}, 'fl.fk_fluid_type', 'ft.id' )
            .leftJoin({so: 'subsoil_object'}, 'fl.fk_subsoil_object', 'so.id' )
            .leftJoin({ct: 'collector_type'}, 'dp.fk_collector_type', 'ct.id' )
            .where('fl.fk_fluid_type', fluidType)
            .where('dp.fk_collector_type', collectorType)
            .where('dp.name', deposit)
            .where('fl.id', '<>', field)
    }

    async searchAnaloguesBySubject(field, fluidType, deposit, collectorType, subject) {
        pool.connect()
        return knex.select({
            id: 'dp.id',
            name: 'dp.name',
            recoveryFactor: 'dp.recovery_factor',
            porosity: 'dp.porosity',
            permeability: 'dp.permeability',
            saturation: 'dp.saturation',
            density: 'dp.density',
            viscosity: 'dp.viscosity',
            collectorType: 'ct.name',
            field: 'fl.name',
            fluidType: 'ft.name',
            subsoilObjectId: 'so.id',
        })
            .from({dp: 'deposit'})
            .leftJoin({ct: 'collector_type'}, 'dp.fk_collector_type', 'ct.id' )
            .leftJoin({fl: 'field'}, 'dp.fk_field', 'fl.id' )
            .leftJoin({ft: 'fluid_type'}, 'fl.fk_fluid_type', 'ft.id' )
            .leftJoin({so: 'subsoil_object'}, 'fl.fk_subsoil_object', 'so.id' )
            .leftJoin({sd: 'subject_federation'}, 'so.fk_subject', 'sd.id' )
            .where('fl.fk_fluid_type', fluidType)
            .where('dp.fk_collector_type', collectorType)
            .where('dp.name', deposit.name)
            .where('fl.id', '<>', field)
            .where('sd.id', subject)
    }

    async addUserHistory(userId) {
        pool.connect()
        return knex('user_history')
            .insert({fk_user: userId})
            .returning('id')
    }

    async addSearchHistory(targetObject, analogObject, historyId) {
        pool.connect()
        return knex('history_search')
            .insert({
                fk_target: targetObject,
                fk_analog: analogObject,
                fk_history: historyId

    })}

    async getSearchTarget(userId) {
        pool.connect()
        return knex.distinct({
            id: 'uh.id',
            date: 'uh.date_search',
            name: 'dp.name',
            recoveryFactor: 'dp.recovery_factor',
            porosity: 'dp.porosity',
            permeability: 'dp.permeability',
            saturation: 'dp.saturation',
            density: 'dp.density',
            viscosity: 'dp.viscosity',
            collectorType: 'ct.name',
            field: 'fl.name',
            fluidType: 'ft.name',
            searchId: 'uh.id',
        })
            .from({uh: 'user_history'})
            .leftJoin({hs: 'history_search'}, 'hs.fk_history', 'uh.id' )
            .leftJoin({dp: 'deposit'}, 'hs.fk_target', 'dp.id')
            .leftJoin({ct: 'collector_type'}, 'dp.fk_collector_type', 'ct.id' )
            .leftJoin({fl: 'field'}, 'dp.fk_field', 'fl.id' )
            .leftJoin({ft: 'fluid_type'}, 'fl.fk_fluid_type', 'ft.id' )
            .where('uh.fk_user', userId)
    }

    async getSearchAnalogue(searchId) {
        pool.connect()
        return knex.select({
            id: 'dp.id',
            name: 'dp.name',
            recoveryFactor: 'dp.recovery_factor',
            porosity: 'dp.porosity',
            permeability: 'dp.permeability',
            saturation: 'dp.saturation',
            density: 'dp.density',
            viscosity: 'dp.viscosity',
            collectorType: 'ct.name',
            field: 'fl.name',
            fluidType: 'ft.name',
        })
            .from({hs: 'history_search'})
            .leftJoin({dp: 'deposit'}, 'hs.fk_analog', 'dp.id')
            .leftJoin({ct: 'collector_type'}, 'dp.fk_collector_type', 'ct.id' )
            .leftJoin({fl: 'field'}, 'dp.fk_field', 'fl.id' )
            .leftJoin({ft: 'fluid_type'}, 'fl.fk_fluid_type', 'ft.id' )
            .where('hs.fk_history', searchId)
            .returning('*')
    }
}

module.exports = new SearchModel();
