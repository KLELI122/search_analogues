const {knexDbConfig} = require('../config/index');
const knex = require('knex')(knexDbConfig);
const pg = require("pg");
const {dbConfig} = require("../config");
const pool = new pg.Pool(dbConfig);

class FieldModel {

    async getFields(id) {
        pool.connect()
        return knex.select({
            id: 'fd.id',
            name: 'fd.name',
            fluidTypeId: 'fl.id',
            fluidType: 'fl.name',
            subsoilObjectId: 'so.id',
            subsoilObject: 'so.name',
            userId: 'fd.fk_user',
        })
            .from({fd: 'field'})
            .leftJoin({fl: 'fluid_type'}, 'fd.fk_fluid_type', 'fl.id' )
            .leftJoin({so: 'subsoil_object'}, 'fd.fk_subsoil_object', 'so.id' )
            .where('fk_user', id)
            .returning('*')
    }

    async getDeposits(id) {
        pool.connect()
        return knex.select({
            id: 'dp.id',
            name: 'dp.name',
            recoveryFactor: 'dp.recovery_factor',
            porosity: 'dp.porosity',
            permeability: 'dp.permeability',
            saturation: 'dp.saturation',
            density: 'dp.density',
            viscosity: 'dp.viscosity',
            collectorTypeId: 'ct.id',
            collectorType: 'ct.name',
        })
            .from({dp: 'deposit'})
            .leftJoin({ct: 'collector_type'}, 'dp.fk_collector_type', 'ct.id')
            .leftJoin({fl: 'field'}, 'dp.fk_field', 'fl.id')
            .where('fl.id', id)
            .returning('*')
    }

    async addField(userId, nameField, fluidId, subsoilId) {
        pool.connect()
        return knex('field')
            .insert({
                name: nameField,
                fk_fluid_type: fluidId,
                fk_subsoil_object: subsoilId,
                fk_user: userId})
            .returning('id')
    }

    async updateField(userId, fieldId, nameField, fluidId, subsoilId) {
        pool.connect()
        return knex('field')
            .update({
                name: nameField,
                fk_fluid_type: fluidId,
                fk_subsoil_object: subsoilId,
                fk_user: userId})
            .where('id', fieldId)
            .returning('id')
    }

    async deleteField(id) {
        pool.connect()
        knex('deposit').where('fk_field', id).del().returning('id');
        return knex('field').where('field.id', id).del().returning('id');
    }

    async addDeposit(fieldId, name, collectorTypeId, recoveryFactor, porosity, permeability, saturation, density, viscosity) {
        pool.connect()
        return knex('deposit')
            .insert({
                name: name,
                recovery_factor: recoveryFactor,
                porosity: porosity,
                permeability: permeability,
                saturation: saturation,
                density: density,
                viscosity: viscosity,
                fk_field: fieldId,
                fk_collector_type: collectorTypeId
            })
            .returning('id')
    }
}

module.exports = new FieldModel();
