const {knexDbConfig} = require('../config/index');
const knex = require('knex')(knexDbConfig);
const pg = require("pg");
const {dbConfig} = require("../config");
const pool = new pg.Pool(dbConfig);

class MapModel {

    async getSubsoilObjects() {
        pool.connect()
        return knex.select({
            id: 'so.id',
            licenseNumber: 'so.license_nu',
            registrationDate: 'so.reg_date',
            expirationDate: 'so.exp_date',
            useType: 'so.use_type',
            mineralResource: 'so.mineral',
            subsoilUser: 'so.user',
            name: 'so.name',
            hyperLink: 'so.hyper_link',
            federalDistrict: 'fd.name',
            subjectFederation: 'sd.name',
            geometry: 'so.text_geometry',
            subjectId: 'sd.id',
            fieldId: 'fl.id'
        })
            .from({so: 'subsoil_object'})
            .leftJoin({sd: 'subject_federation'}, 'so.fk_subject', 'sd.id' )
            .leftJoin({fd: 'federal_district'}, 'sd.fk_federal_district', 'fd.id' )
            .leftJoin({fl: 'field'}, 'fl.fk_subsoil_object', 'so.id' )
    }

    async getIdSubsoilObjectByFieldId(id) {
        pool.connect()
        return knex.select({
            id: 'so.id',
            licenseNumber: 'so.license_nu',
            registrationDate: 'so.reg_date',
            expirationDate: 'so.exp_date',
            useType: 'so.use_type',
            mineralResource: 'so.mineral',
            subsoilUser: 'so.user',
            name: 'so.name',
            hyperLink: 'so.hyper_link',
            federalDistrict: 'fd.name',
            subjectFederation: 'sd.name',
        })
            .from({so: 'subsoil_object'})
            .leftJoin({sd: 'subject_federation'}, 'so.fk_subject', 'sd.id' )
            .leftJoin({fd: 'federal_district'}, 'sd.fk_federal_district', 'fd.id' )
            .leftJoin({fl: 'field'}, 'fl.fk_subsoil_object', 'so.id' )
            .whereRaw(`fl.id = ${id}`)

    }
}

module.exports = new MapModel();
